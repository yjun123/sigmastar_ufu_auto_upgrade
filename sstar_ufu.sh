#!/bin/bash
# Copyright (C), 2015-2023, Sunny OIT. CO., Ltd.
# Filename: sigmastar_ufu_auto_upgrade.sh
# Author: yanjun (zngyanj@sunnyoptical.com)
# Date: 2023/04/25
# Description: SigmaStar 批量烧录固件脚本(针对启动默认启动UBoot UFU的模块)
# Usage: ./sigmastar_ufu_auto_upgrade.sh --help

########## Supported Device #########
## SunnyPalm
### USB VID & PID
SUNNYPALM_VID="35c2"
SUNNYPALM_PID="0002"

SUNNYPALM_HX_PV200P_VID="4856"
SUNNYPALM_HX_PV200P_PID="0266"

### UVC Ext Enter UFU
PALM_CAMERA_EXTENSION_UNIT_ID="2"
PALM_CAMERA_EXTENSION_UNIT_QUERY_CS="1"
PALM_CAMERA_XU_CMD_SET_UFU_MODE='0x55aaE00524554655000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000f8'
PALM_CAMERA_XU_CMD_RESET_DEVICE='0x55aa01050100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000006'
# ********************************** #

## WePalm
### USB VID & PID
WEPALM_VID="35c2"
WEPALM_PID="0001"
### UVC Ext Enter UFU
# same as SunnyPalm
# ***************** #

## Mars05d
### USB VID & PID
MARS05D_VID="1d6b"
MARS05D_PID="0102"
MARS05D_EXTENSION_UNIT_ID="2"
MARS05D_EXTENSION_UNIT_QUERY_CS="1"
### UVC Ext Enter UFU
MARS05D_XU_CMD_SET_UFU_MODE='0x06000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'
MARS05D_XU_CMD_RESET_DEVICE='0x05000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'

## HSP009
### USB VID & PID
HSP009_VID="1d6b"
HSP009_PID="010c"
### UVC Ext Enter UFU
HSP009_EXTENSION_UNIT_ID="2"
HSP009_EXTENSION_UNIT_QUERY_CS="1"
HSP009_XU_CMD_SET_UFU_MODE='0x06000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'
HSP009_XU_CMD_RESET_DEVICE='0x05000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'
# ***************** #
########## Supported Device #########

######### Config Variable #######
USB_DOWNLOAD_TOOL="usbdownload"
SG_DEV=/dev/sg1
MSTAR_UFU_VID="1b20"
MSTAR_UFU_PID="0300"
UVC_TOOL="uvcdynctrl"
UVC_MEDIA_DEFAULT_DEV=/dev/media0
######### Config Variable #######

########## LOG CONFIG ###########
LOG_LEVEL=2
function log_dbg(){
  content="[DBG]: $(date '+%Y-%m-%d %H:%M:%S')"
  [ $LOG_LEVEL -le 1  ] && echo -e "\033[34m"  ${content} "\033[0m $@"
}

function log_info(){
  content="[INFO]: $(date '+%Y-%m-%d %H:%M:%S')"
  [ $LOG_LEVEL -le 2  ] && echo -e "\033[32m"  ${content} "\033[0m $@"
}

function log_warn(){
  content="[WARN]: $(date '+%Y-%m-%d %H:%M:%S')"
  [ $LOG_LEVEL -le 3  ] && echo -e "\033[33m"  ${content} "\033[0m $@"
}

function log_err(){
  content="[ERR]: $(date '+%Y-%m-%d %H:%M:%S')"
  [ $LOG_LEVEL -le 4  ] && echo -e "\033[31m"  ${content} "\033[0m $@"
}
########## LOG CONFIG ###########


function usage(){
	echo "Usage: $0 [cam_type] [PATH to UFU fw dir] "
	echo "-h, --help	show usage"
    echo "example: sudo ./sigmastar_ufu_auto_upgrade.sh SunnyPalm /home/yjun/Image/UFU"
	exit 0
}

function disable_reset()
{
    ufu_path=$1
    sed -i 's|reset|#reset|g' ${ufu_path}/auto_update.txt
}

function check_upgrade_tool()
{
    file="$1"

    if [ ! -f "$file" ];then
        log_err "$file is not existed!"
        exit 1
    fi

    if [ -x "$file" ];then
        log_dbg "$file is executable."
        return 0;
    fi

    log_err "$file is not executable!"
    return 1
}

function is_usb_mstar_existed()
{
    if grep "Vendor=${MSTAR_UFU_VID} ProdID=${MSTAR_UFU_PID}" \
        /sys/kernel/debug/usb/devices >> /dev/null ;then
        log_dbg "Mstar UFU is existed."
        return 0
    fi

    log_dbg "Mstar UFU is not existed!"
    return 1
}

function ufu_fw_download()
{
    cd "$*" || exit

    
    log_info "wait for ${SG_DEV}"
    while [ ! -c ${SG_DEV} ];
    do
        sleep 0.5
    done
    
    # sed -i 's|reset|#reset|g' auto_update.txt

    log_info "${SG_DEV}: downloading ..."
    if ./${USB_DOWNLOAD_TOOL} ${SG_DEV};then
        log_info "download ok!"
        return 0
    fi

    return 1
}

usb_detected=1
media_detected=1
function is_cam_usb_existed()
{
    if ! grep "Vendor=${g_cam_vid} ProdID=${g_cam_pid}" \
        /sys/kernel/debug/usb/devices >> /dev/null ;then
        if [ $usb_detected -ne 0 ];then
            log_warn "${g_cam_type} USB is not detected!"
            usb_detected=0
        fi
        return 1
    else
        if [ $usb_detected -ne 1 ];then
            log_info "${g_cam_type} USB is detected."
            usb_detected=1
        fi
    fi

    if [ ! -c "$UVC_MEDIA_DEFAULT_DEV" ];then
        if [ $media_detected -ne 0 ];then
            log_err "$UVC_MEDIA_DEFAULT_DEV is not found. please check up virual machine."
            media_detected=0
        fi
        return 1
    else
        if [ $media_detected -ne 1 ];then
            log_info "$UVC_MEDIA_DEFAULT_DEV is found."
            media_detected=1
        fi
    fi

    log_dbg "${g_cam_type} is detected."
    return 0
}

function uvc_extension_ctrl()
{
    req="$1"
    # sudo uvcdynctrl --set_raw=2:1 '(LE)0x55aaE00524554655000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000f8'
    ${UVC_TOOL} --set_raw=${g_cam_uvc_ext_id}:${g_cam_uvc_ext_cs} "(LE)${req}" >> /dev/null
}

function set_cam_to_ufu()
{
    case $g_cam_type in
        "sunnypalm")
            g_cam_uvc_ext_id="$PALM_CAMERA_EXTENSION_UNIT_ID"
            g_cam_uvc_ext_cs="$PALM_CAMERA_EXTENSION_UNIT_QUERY_CS"
            uvc_extension_ctrl "$PALM_CAMERA_XU_CMD_SET_UFU_MODE"
            sleep 0.5
            uvc_extension_ctrl "$PALM_CAMERA_XU_CMD_RESET_DEVICE"
            sleep 2
        ;;
        "sunnypalm-hx")
            g_cam_uvc_ext_id="$PALM_CAMERA_EXTENSION_UNIT_ID"
            g_cam_uvc_ext_cs="$PALM_CAMERA_EXTENSION_UNIT_QUERY_CS"
            uvc_extension_ctrl "$PALM_CAMERA_XU_CMD_SET_UFU_MODE"
            sleep 0.5
            uvc_extension_ctrl "$PALM_CAMERA_XU_CMD_RESET_DEVICE"
            sleep 2
        ;;
        "wepalm")
            g_cam_uvc_ext_id="$PALM_CAMERA_EXTENSION_UNIT_ID"
            g_cam_uvc_ext_cs="$PALM_CAMERA_EXTENSION_UNIT_QUERY_CS"
            uvc_extension_ctrl "$PALM_CAMERA_XU_CMD_SET_UFU_MODE"
            sleep 0.5
            uvc_extension_ctrl "$PALM_CAMERA_XU_CMD_RESET_DEVICE"
            sleep 2
        ;;
        "mars05d")
            return 0
        ;;
        "hsp009")
            g_cam_uvc_ext_id="$HSP009_EXTENSION_UNIT_ID"
            g_cam_uvc_ext_cs="$HSP009_EXTENSION_UNIT_QUERY_CS"
            uvc_extension_ctrl "$HSP009_XU_CMD_SET_UFU_MODE"
            sleep 0.5
            uvc_extension_ctrl "$HSP009_XU_CMD_RESET_DEVICE"
            sleep 2
        ;;
        *)
            log_err "Wrong camera type $cam_type"
            return 1
        ;;
    esac
    return 0
}


function is_cam_existed()
{
    local cam_type=${1,,}

    case $cam_type in
        "sunnypalm")
            g_cam_vid="$SUNNYPALM_VID"
            g_cam_pid="$SUNNYPALM_PID"
        ;;
        "sunnypalm-hx")
            g_cam_vid="$SUNNYPALM_HX_PV200P_VID"
            g_cam_pid="$SUNNYPALM_HX_PV200P_PID"
        ;;
        "wepalm")
            g_cam_vid="$WEPALM_VID"
            g_cam_pid="$WEPALM_PID"
        ;;
        "mars05d")
            g_cam_vid="$MARS05D_VID"
            g_cam_pid="$MARS05D_PID"
        ;;
        "hsp009")
            g_cam_vid="$HSP009_VID"
            g_cam_pid="$HSP009_PID"
        ;;
        *)
            log_err "Wrong camera type $cam_type"
            return 1
        ;;
    esac
    
    g_cam_type="$cam_type"
    if ! is_cam_usb_existed;then
        return 1
    fi

    return 0
}

function beep()
{
    echo -n -e "\a"
}

MSTAR_UFU_NOT_FOUND=1
function main(){
    if [ $# -lt 2 ];then
        log_err "wrong param!"
        usage
        return 1
    fi

    local cam_type=$1
    local ufu_path=$2
    local others=${3:-single}
    local partations=${4:-all}
    log_info "enter ${others} mode"
    log_warn "please plug the device..."
    while true
    do
        if is_cam_existed "$cam_type";then
            log_info "Set Device to UFU Mode"
            set_cam_to_ufu
        fi

        if is_usb_mstar_existed;then
            if [ "$MSTAR_UFU_NOT_FOUND" -eq 1 ];then
                MSTAR_UFU_NOT_FOUND=0
                if ! check_upgrade_tool ${ufu_path}/${USB_DOWNLOAD_TOOL};then
                    chmod u+x "${USB_DOWNLOAD_TOOL}"
                fi
                if [ "$others" == "loop" ];then
                    disable_reset "$ufu_path"
                fi
                if [ "$partations" == "customer" ]
                then
                    if ! head -n 1 ${ufu_path}/auto_update.txt | grep -q '^.*customer'
                    then
                        sed -i -e '1,11 {/customer/!d}' ${ufu_path}/auto_update.txt
                        log_warn "set auto_update.txt only upgrade customer"
                    fi
                fi
                ufu_fw_download "$ufu_path"
                beep
                if [ "$others" == "single" ];then
                    log_info "sstar ufu exits"
                    return
                fi
                sleep 1
            elif [ "$MSTAR_UFU_NOT_FOUND" -eq 0 ];then
                MSTAR_UFU_NOT_FOUND=2
                log_warn "Please unplug this device and plug next device!"
            elif [ "$MSTAR_UFU_NOT_FOUND" -eq 2 ];then
                beep
                sleep 1
            fi

        else
            if [ $MSTAR_UFU_NOT_FOUND -ne 1 ];then
                MSTAR_UFU_NOT_FOUND=1
                log_err "Mstar UFU device not found."
                beep
                sleep 1
            fi
            continue
        fi
    done
}

if [ $UID -ne 0 ];then
  log_err "only root can do that!"
  exit
fi
main "$@"
