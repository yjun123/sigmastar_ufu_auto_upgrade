### sigmastar ufu auto upgrade

#### 介绍

星宸 UFU 批量自动更新脚本

- 功能
    
    对于批量的设备需要更新固件, 只需在每次更新完毕(会有提示音),重新插入新设备即可

    可用于正常设备(启动默认不进入UBOOT UFU,正常启动的设备)使用

    可用于无程序设备(启动默认进入UBOOT UFU)使用

#### 使用说明

```
# 以 ROOT 用户执行

#                                      相机类型     UFU 固件路径
$ sudo ./sigmastar_ufu_auto_upgrade.sh SunnyPalm /home/yjun/Image/UFU
```

#### 添加新的设备

- 添加 新设备相机的PID VID

- 添加 新设备由正常模式进入UFU模式的 UVC 拓展通道命令

- 在 `is_cam_existed` 函数添加 新的相机类型判断(PID&VID)

- 在 `set_cam_to_ufu` 函数添加 新相机的设置UFU模式的步骤(UVC Ext)

#### TODO

- 新设备添加更简便
